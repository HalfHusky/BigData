using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ConfirmSave : MonoBehaviour
{
    public List<Data> Data = new List<Data>();

    public int age;
    private string textage;
    public string textsex;
    [SerializeField] private TextMeshProUGUI agetext;
    [SerializeField] private TextMeshProUGUI sextext;
    [SerializeField] private GameObject start;
    [SerializeField] private GameObject game;
    [SerializeField] private GameObject textOnStart;

    
    public void SaveData()
    {
        textage = agetext.text;
        textage = textage.Trim().Replace("\u200B", "");
        textsex = sextext.text;

        //foreach (char c in textsex)
        //{
        //    Debug.Log("Character: " + c + " ASCII Value: " + (int)c);
        //}

        Debug.Log(agetext.text);
        if (!int.TryParse(textage, out age))
        {
            Debug.LogError("Input string is not a valid integer.");
            return;
        }
        Data data = new Data();
        Data.Add(data);
        data.age = age;
        data.sex = sextext.text;
        textOnStart.SetActive(true);
        game.SetActive(true);
        start.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (Data item in Data)
            {
                Debug.Log(item.age + ", " + item.sex+"\n");
            }
        }
    }
}
