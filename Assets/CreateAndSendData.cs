using Google.Cloud.BigQuery.V2;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Google.Apis.Auth.OAuth2;
using UnityEngine;

public class CreateAndSendData : MonoBehaviour
{
    [SerializeField] private Logic logic;

    [System.Serializable]
    public class TestData
    {
        public int age;
        public string sex;
        public string date;
        public float timer;
        public string color;
    }

    [SerializeField] public List<TestData> testDatas = new List<TestData>();

    public void SaveData()
    {
        testDatas.Clear();
        for (int i = 0; i < logic.times.Count; i++)
        {
            TestData data = new TestData
            {
                timer = logic.times[i],
                date = logic.date[i],
                age = logic.ages[i],
                color = logic.colors[i],
                sex = logic.sexes[i]
            };
            testDatas.Add(data);
        }

        if (testDatas.Count == 0) return;

        string json = JsonConvert.SerializeObject(testDatas, Newtonsoft.Json.Formatting.Indented);

        string filePath = Application.persistentDataPath + "/testData.json";

        // Write the JSON data to the file
        File.WriteAllText(filePath, json);

        Debug.Log("JSON data saved to: " + filePath);

        // Conversion of JSON to CSV
        var csvContent = new StringBuilder();
        csvContent.AppendLine("age,sex,date,timer,color");
        foreach (var data in testDatas)
        {
            csvContent.AppendLine($"{data.age},{data.sex},{data.date},{data.timer},{data.color}");
        }

        // Write to CSV file
        var csvFilePath = Application.persistentDataPath + "/testData2.csv";
        File.WriteAllText(csvFilePath, csvContent.ToString());

        Debug.Log("CSV data saved to: " + csvFilePath);

        // Use JSON file to update BigQuery
        UpdateBigQuery(filePath);
    }

    private async void UpdateBigQuery(string jsonFilePath)
    {
        string projectId = "bigquery-zaliczenie";
        string datasetId = "Wyniki";
        string tableId = "TabelaWynikow";
        
        TextAsset keyFile = Resources.Load<TextAsset>("bigquery-zaliczenie-5d06f6fbb54d");
        string keyJson = keyFile.text;

        // Use the key file to authenticate with Google Cloud
        GoogleCredential credential = GoogleCredential.FromJson(keyJson);
        var client = BigQueryClient.Create(projectId, credential);

        var tableReference = client.GetTableReference(datasetId, tableId);

        var rows = new List<BigQueryInsertRow>();

        // Read data from the JSON file
        string json = File.ReadAllText(jsonFilePath);
        var records = JsonConvert.DeserializeObject<List<TestData>>(json);

        // Validate the records to ensure no confusion between fields
        foreach (var record in records)
        {
            Debug.Log($"Read record: Age={record.age}, Sex={record.sex}, Date={record.date}, Timer={record.timer}, Color={record.color}");

            var row = new BigQueryInsertRow
            {
                { "age", record.age },
                { "sex", record.sex },
                { "date", record.date },
                { "timer", record.timer },
                { "color", record.color }
            };
            rows.Add(row);
        }

        if (rows.Count > 0)
        {
            var insertOptions = new InsertOptions();
            var job = await client.InsertRowsAsync(tableReference, rows, insertOptions);
            if (job != null)
            {
                Debug.Log("Data inserted into BigQuery successfully.");
            }
            else
            {
                Debug.LogError("Error inserting data into BigQuery.");
            }
        }
    }
}
