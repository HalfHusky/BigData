using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEditor.Build;
using UnityEngine;

public class Logic : MonoBehaviour
{
    [SerializeField] private ConfirmSave confirmSave;
    [SerializeField] private GameObject textOnStart;
    [Header("          ")]
    [SerializeField] private GameObject cover;
    [SerializeField] private GameObject parent;
    [Header("          ")]
    [SerializeField] private float timeMinToShow;
    [SerializeField] private float timeMaxToShow;
    [SerializeField] private float timerToShow;
    [Header("          ")]
    [SerializeField] private float timeMaxToClose;
    [SerializeField] private float timerToClose;
    [Header("          ")]
    [SerializeField] private float timerReaction;
    [Header("          ")]
    [SerializeField] private bool isVisible = false;
    [SerializeField] private bool isCounting = false;
    [Header("          ")]
    public List<float> times;
    public List<string> colors;
    public List<int> ages;
    public List<string> sexes;
    public List<string> date;
    [SerializeField] private string colorOfChild;
    public void StartGame()
    {
        cover.SetActive(true);
        isVisible = false;
        for (int i = 0; i < parent.transform.childCount; i++)
        {            
            parent.transform.GetChild(i).gameObject.SetActive(false);
        }
        int x = UnityEngine.Random.Range(0, parent.transform.childCount);
        parent.transform.GetChild(x).gameObject.SetActive(true);
        colorOfChild = SelectedChildColor(x);
        timerToShow = RandomTimer();
    }
    private float RandomTimer()
    {
        float x = UnityEngine.Random.Range(timeMinToShow, timeMaxToShow);
        return x;
    }
    private void ChoseChild()
    {
        int x = UnityEngine.Random.Range(0, parent.transform.childCount + 1);
        Debug.Log(x.ToString());
    }
    private void Reveal()
    {
        if (!isCounting) return;

        if (isVisible)
        {
            timerReaction += Time.deltaTime;
            timerToClose -= Time.deltaTime;
        }
        else
        {
            
            timerToShow -= Time.deltaTime;
        }
        if (timerToClose <=0)
        {
            timerReaction = 0;
            StartGame();
            timerToClose = timeMaxToClose;
        }
        

        if (timerToShow<=0)
        {
            //show
            timerToClose = timeMaxToClose;
            timerToShow = timeMaxToShow;
            timerReaction = 0;
            isVisible = true;
            timerToClose = timeMaxToClose;
            cover.SetActive(false);
        }
    }
    public void StopPlaying()
    {
        isCounting = false;
    }
    private void Click()
    {
        if(!isVisible) return;
        if (Input.anyKeyDown)
        {
            times.Add(timerReaction);            
            colors.Add(colorOfChild);
            ages.Add(confirmSave.age);
            sexes.Add(confirmSave.textsex);
            date.Add(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            StartGame();
        }
    }
    private string SelectedChildColor(int colorIndex)
    {
        switch (colorIndex)
        {
            case 0:
                return "Red";                
            case 1:
                return "Blue";
            case 2:
                return "Yellow";
            case 3:
                return "edges";
            default:
                return "";
        }
    }
    private void Update()
    {
        if (isCounting)
        {
            Reveal();            
            Click();
        }
        if (Input.GetKeyDown(KeyCode.Return)&& !isCounting)
        {
            textOnStart.SetActive(false);
            isCounting = true;
            StartGame();
        }
        
    }
  
}
